<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Task */

(Yii::$app->user->identity->getId() === '101') ? $this->title = 'Редактирование обращения' : $this->title = 'Решение по обращению';

$this->params['breadcrumbs'][] = ['label' => 'Обращения', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="task-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php 
    	if(Yii::$app->user->identity->getId() === '101') {
	    	echo $this->render('_form', [
	        	'model' => $model,
	    	]);
		} else {
			echo $this->render('_form_ministry', [
	        	'model' => $model,
	    	]);
		} ?>
</div>
