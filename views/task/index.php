<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TaskSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Обращения';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="task-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php if(Yii::$app->user->identity->getId() === '101'): ?>
    <p>
        <?= Html::a('Создать обращение', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <h4>&rarr; Новые и рассмотренные</h4>
    <?php else: ?>
    <h4>&rarr; Новые</h4>
    <?php endif; ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'summary' => "Отображено {begin} - {end} из {totalCount} обращений",
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'name',
            'text:ntext',
            'date',
            [
                'attribute' => 'resolutions',
                'value' => 'resolutions.resolution_name'
            ],
            // 'comment:ntext',
            // 'is_new',
            // 'is_executed',
            // 'execution_date',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}<br/>{update}<br/>{delete}'
            ],
        ],
    ]); ?>

    <br />
    <?php if(Yii::$app->user->identity->getId() === '101'): ?>
    <h4>&rarr; Исполненные</h4>
    <?php else: ?>
    <h4>&rarr; Рассмотренные и исполненные</h4>
    <?php endif; ?>

    <?= GridView::widget([
        'dataProvider' => $dataProviderExecutedOnly,
        'filterModel' => $searchModel,
        'summary' => "Отображено {begin} - {end} из {totalCount} обращений",
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'name',
            'text:ntext',
            'date',
            [
                'attribute' => 'resolutions',
                'value' => 'resolutions.resolution_name'
            ],
            'execution_date',
            // 'comment:ntext',
            // 'is_new',
            // 'is_executed',
            // 'execution_date',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}<br/>{delete}',
            ],
        ],
    ]); ?>

</div>
