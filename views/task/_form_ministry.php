<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Task */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="task-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'readOnly'=>true]) ?>

    <?= $form->field($model, 'text')->textarea(['rows' => 6,'readOnly'=>true]) ?>

    <?= $form->field($model, 'date')->textInput(['readOnly'=>true]) ?>

    <?= $form->field($model, 'resolution')->dropDownList(['1'=>'Принять','2'=>'Отказать','3'=>'Перезвоню','4'=>'Другое']) ?>

    <?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
