<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Task */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Обращения', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="task-view">

<h1><?= Html::encode($this->title) ?></h1>

<?php if($model->resolution == 6):?>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
            'text:ntext',
            'date',
            [
                'attribute' => 'resolutions',
                'value' => $model->resolutions->resolution_name,
            ],
            'comment:ntext',
            'execution_date',
        ],
    ]) ?>
<?php else: ?>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
            'text:ntext',
            'date',
            [
                'attribute' => 'resolutions',
                'value' => $model->resolutions->resolution_name,
            ],
            'comment:ntext',
        ],
    ]) ?>
<?php endif; ?>

<?php if(Yii::$app->user->identity->getId() === '101'): ?>
    <p>
        <?= Html::a('Обновить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Удалить обращение?',
                'method' => 'post',
            ],
        ]) ?>
        <?php
            if($model->is_executed == 0 && $model->resolution != 5) {
                echo Html::a('Исполнить', ['complete', 'id' => $model->id], [
                    'class' => 'btn btn-success',
                    'data' => [
                        'confirm' => 'Обращение исполнено?',
                        'method' => 'post',
                    ],
                ]); 
            }
        ?>
    </p>
<?php else: ?>
    <?php if($model->resolution == 5):?>
        <?= Html::a('Рассмотреть', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    <?php endif ?>
<?php endif ?>

</div>
