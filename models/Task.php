<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "task".
 *
 * @property integer $id
 * @property string $name
 * @property string $text
 * @property string $date
 * @property string $resolution
 * @property string $comment
 * @property string $is_new
 * @property string $is_executed
 * @property string $execution_date
 */
class Task extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'task';
    }

    /**
     * @inheritdoc
     */

    public function getResolutions()
    {
        return $this->hasOne(Resolutions::className(), ['id' => 'resolution']);
    }

    public function rules()
    {
        return [
            [['name', 'text'], 'required'],
            [['text', 'comment'], 'string'],
            [['date', 'execution_date'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['resolution'], 'integer', 'max' => 50],
            [['is_new', 'is_executed'], 'integer', 'max' => 1]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'text' => 'Текст обращения',
            'date' => 'Дата регистрации',
            'resolution' => 'Решение',
            'resolutions' => 'Статус',
            'comment' => 'Коментарий',
            'is_new' => 'Is New',
            'is_executed' => 'Is Executed',
            'execution_date' => 'Дата исполнения',
        ];
    }
}
