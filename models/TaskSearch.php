<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Task;

/**
 * TaskSearch represents the model behind the search form about `app\models\Task`.
 */
class TaskSearch extends Task
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['name', 'text', 'date', 'resolution', 'comment', 'is_new', 'is_executed', 'execution_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params,$flag)
    {
        $query = Task::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'date' => $this->date,
            'execution_date' => $this->execution_date,
        ]);

        if($flag===0) {
            $query->andFilterWhere(['like', 'name', $this->name])
                ->andFilterWhere(['like', 'text', $this->text])
                //->andFilterWhere(['like', 'resolution', $this->resolution])
                ->andFilterWhere(['not like', 'resolution', 6])
                ->andFilterWhere(['like', 'comment', $this->comment])
                ->andFilterWhere(['like', 'is_new', $this->is_new])
                ->andFilterWhere(['like', 'is_executed', $this->is_executed]);
        } elseif($flag===1) {
            $query->andFilterWhere(['like', 'name', $this->name])
                ->andFilterWhere(['like', 'text', $this->text])
                //->andFilterWhere(['like', 'resolution', $this->resolution])
                ->andFilterWhere(['like', 'resolution', 6])
                ->andFilterWhere(['like', 'comment', $this->comment])
                ->andFilterWhere(['like', 'is_new', $this->is_new])
                ->andFilterWhere(['like', 'is_executed', $this->is_executed]);
        } elseif($flag===2) {
            $query->andFilterWhere(['like', 'name', $this->name])
                ->andFilterWhere(['like', 'text', $this->text])
                //->andFilterWhere(['like', 'resolution', $this->resolution])
                ->andFilterWhere(['like', 'resolution', 5])
                ->andFilterWhere(['like', 'comment', $this->comment])
                ->andFilterWhere(['like', 'is_new', $this->is_new])
                ->andFilterWhere(['like', 'is_executed', $this->is_executed]);
        } elseif($flag===3) {
            $query->andFilterWhere(['like', 'name', $this->name])
                ->andFilterWhere(['like', 'text', $this->text])
                //->andFilterWhere(['like', 'resolution', $this->resolution])
                ->andFilterWhere(['not like', 'resolution', 5])
                ->andFilterWhere(['like', 'comment', $this->comment])
                ->andFilterWhere(['like', 'is_new', $this->is_new])
                ->andFilterWhere(['like', 'is_executed', $this->is_executed]);
        } 

        return $dataProvider;
    }
}
