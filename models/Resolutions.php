<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "resolutions".
 *
 * @property integer $id
 * @property string $name
 */
class Resolutions extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'resolutions';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['resolution_name'], 'required'],
            [['resolution_name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'resolution_name' => 'Resolution Name',
        ];
    }
}
